# white-october/[pagerfanta-bundle](https://phppackages.org/p/white-october/pagerfanta-bundle)

Bundle to use Pagerfanta Pagination with Symfony

Unofficial Demo and Howto

> This package is abandoned and no longer maintained. The author suggests using the [babdev/pagerfanta-bundle](https://gitlab.com/php-packages-demo/babdev-pagerfanta-bundle) package instead. 

## Other pagers
* See also [notes-on-computer-programming-languages/php](https://gitlab.com/notes-on-computer-programming-languages/php)
* [paginator](https://phppackages.org/s/paginator) (PHPPackages.org)
* nette/utils
* [knplabs/knp-paginator-bundle](https://phppackages.org/p/knplabs/knp-paginator-bundle) <br/>
  ([php-packages-demo/knplabs-knp-paginator-bundle](https://gitlab.com/php-packages-demo/knplabs-knp-paginator-bundle))
* illuminate/pagination
  * https://laravel.com/api/5.3/Illuminate/Pagination.html
  * https://laravel-guide.readthedocs.io/en/latest/pagination/
* knplabs/knp-components
* laminas/laminas-paginator